import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './front-end/home/home.component';
import { RouterModule, Routes } from '@angular/router';

import { FrontEndComponent } from './front-end/front-end.component';

const appRoutes: Routes = [
  { path: '', component: FrontEndComponent },
  
  
];
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    
    FrontEndComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
